"""sennparrot_example: just a dummy module"""
__version__ = '0.0.3'

from .parrot import EASTER_EGG, LANNEASTER_EGG

__all__ = ['EASTER_EGG', 'LANNEASTER_EGG']
