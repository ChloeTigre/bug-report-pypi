from senn_parrot_example.parrot import wac, caw

def test_wac():
    assert wac() == -42

def test_caw():
    assert caw() == 42
